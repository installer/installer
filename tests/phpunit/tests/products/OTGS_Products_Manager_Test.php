<?php

use OTGS\Installer\Products\ExternalProductsUrls;
use PHPUnit\Framework\TestCase;

class OTGS_Products_Manager_Test extends TestCase {

	/**
	 * @var WP_Installer_Channels
	 */
	private $installer_channels;

	/**
	 * @var OTGS_Products_Config_Xml
	 */
	private $products_config_xml;

	/**
	 * @var OTGS_Installer_Logger_Storage
	 */
	private $logger_storage;

	/**
	 * @var ExternalProductsUrls
	 */
	private $externalProductUrls;

	public function __construct() {
		$this->products_config_xml = $this->createMock(OTGS_Products_Config_Xml::class);
		$this->installer_channels = $this->createMock(WP_Installer_Channels::class);
		$this->logger_storage = $this->createMock(OTGS_Installer_Logger_Storage::class);
		$this->externalProductUrls = $this->createMock(ExternalProductsUrls::class);

		parent::__construct();
	}

	/**
	 * @test
	 */
	public function it_logs_exception_when_unable_to_fetch_products_url_from_endpoint() {
		$site_key = 'test_site_key';
		$site_url = 'http://site-url.com';
		$repository_id = 'test_repository';
		$api_url = 'http://api-url.com';

		$error_message = 'Unable to connect to url.';

		$this->externalProductUrls
			->method('fetchProductUrl')
			->with(
				$this->equalTo($repository_id),
				$this->equalTo($api_url),
				$this->equalTo($site_key),
				$this->equalTo($site_url)
			)
			->will( $this->throwException( new Exception( $error_message ) ) );

		$this->products_config_xml
			->method('get_products_api_urls')
			->willReturn([$repository_id => $api_url]);

		$this->installer_channels
			->method( 'get_channel' )
			->with( $repository_id )
			->willReturn( WP_Installer_Channels::CHANNEL_PRODUCTION );

		$subject = $this->get_subject();

		$expected_log = new OTGS_Installer_Log();
		$expected_log->set_component(OTGS_Installer_Logger_Storage::COMPONENT_PRODUCTS_URL);
		$expected_log->set_response( "Installer cannot contact our updates server to get information about the available products of $repository_id and check for new versions. Error message: $error_message" );

		$this->logger_storage
			->expects($this->once())
			->method('add')
			->with(
				$this->equalTo( $expected_log )
			);

		$subject->get_products_url( $repository_id, $site_key, $site_url, false );
	}

	/**
	 * @test
	 */
	public function it_allows_product_url_to_be_overriden_by_constant() {
		$site_key = 'test_site_key';
		$site_url = 'http://site-url.com';
		$repository_id = 'repository_id';

		$expected_products_url = 'http://some-other-url.com/xyz';

		define( "OTGS_INSTALLER_REPOSITORY_ID_PRODUCTS", $expected_products_url );

		$subject = $this->get_subject();
		$result = $subject->get_products_url( $repository_id, $site_key, $site_url, false );

		$this->assertEquals( $expected_products_url, $result );
	}

	/**
	 * @test
	 */
	public function it_does_not_return_product_url_from_local_config_when_site_key_empty() {
		$site_key =  false;
		$site_url = 'http://site-url.com';
		$repository_id = 'test_repository';

		$subject = $this->get_subject();

		$result = $subject->get_products_url( $repository_id, $site_key, $site_url, false );
		$this->assertEquals( null, $result );
	}

	/**
	 * @test
	 */
	public function it_returns_product_url_from_endpoint() {
		$repository_products_url = 'products_url';

		$site_key = 'test_site_key';
		$site_url = 'http://site-url.com';
		$repository_id = 'test_repository';
		$api_url = 'http://api-url.com';

		$this->externalProductUrls
			->method('fetchProductUrl')
			->with(
				$this->equalTo($repository_id),
				$this->equalTo($api_url),
				$this->equalTo($site_key),
				$this->equalTo($site_url)
			)
			->willReturn( $repository_products_url );

		$this->products_config_xml
			->method('get_products_api_urls')
			->willReturn([$repository_id => $api_url]);

		$this->installer_channels
			->method( 'get_channel' )
			->with( $repository_id )
			->willReturn( WP_Installer_Channels::CHANNEL_PRODUCTION );

		$subject = $this->get_subject();

		$result = $subject->get_products_url( $repository_id, $site_key, $site_url, false );
		$this->assertEquals( $repository_products_url, $result );
	}

	/**
	 * @test
	 */
	public function it_returns_product_url_from_local_configuration_file() {
		$site_key = 'test_site_key';
		$site_url = 'http://site-url.com';
		$repository_id = 'test_repository';
		$repository_products_url = 'products_url';
		$api_url = 'api_url';

		$this->products_config_xml
			->method('get_products_api_urls')
			->willReturn([$repository_id => $api_url]);

		$this->externalProductUrls
			->method('fetchProductUrl')
			->with(
				$this->equalTo($repository_id),
				$this->equalTo($api_url),
				$this->equalTo($site_key),
				$this->equalTo($site_url)
			)
			->willReturn( null );

		$this->products_config_xml
			->method('get_repository_products_url')
			->with(
				$this->equalTo($repository_id)
			)
			->willReturn($repository_products_url);

		$subject = $this->get_subject();

		$result = $subject->get_products_url( $repository_id, $site_key, $site_url, false );
		$this->assertEquals( $repository_products_url, $result );
	}

	/**
	 * @test
	 */
	public function it_returns_product_url_from_xml_if_bucket_is_bypassed() {
		$site_key = 'test_site_key';
		$site_url = 'http://site-url.com';
		$repository_id = 'test_repository';
		$local_products_url = 'local_products_url';

		$this->externalProductUrls
			->expects( $this->never() )
			->method('fetchProductUrl');

		$this->products_config_xml
			->method('get_repository_products_url')
			->with(
				$this->equalTo($repository_id)
			)
			->willReturn($local_products_url);

		$subject = $this->get_subject();

		$this->assertEquals(
			$local_products_url,
			$subject->get_products_url( $repository_id, $site_key, $site_url, true )
		);
	}

	/**
	 * @test
	 */
	public function it_returns_product_url_from_xml_if_channel_not_production() {
		$site_key = 'test_site_key';
		$site_url = 'http://site-url.com';
		$repository_id = 'test_repository';
		$local_products_url = 'local_products_url';

		$this->installer_channels
			->method( 'get_channel' )
			->with( $repository_id )
			->willReturn( WP_Installer_Channels::CHANNEL_BETA );

		$this->externalProductUrls
			->expects( $this->never() )
			->method('fetchProductUrl');

		$this->products_config_xml
			->method('get_repository_products_url')
			->with(
				$this->equalTo($repository_id)
			)
			->willReturn($local_products_url);

		$subject = $this->get_subject();

		$this->assertEquals(
			$local_products_url,
			$subject->get_products_url( $repository_id, $site_key, $site_url, false )
		);
	}

	/**
	 * @return OTGS_Products_Manager
	 */
	private function get_subject() {
		$subject = new OTGS_Products_Manager(
			$this->products_config_xml,
			$this->installer_channels,
			$this->logger_storage,
			$this->externalProductUrls
		);

		return $subject;
	}

}
