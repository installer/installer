<?php

namespace OTGS\Installer\Tests\Products;


use OTGS\Installer\Products\ExternalProductsUrls;
use OTGS_Products_Bucket_Repository;
use OTGS_Products_Config_Db_Storage;

class ExternalProductsUrlsTest extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_fetches_product_url_from_local_config() {
		$products_config_storage = $this->createMock(OTGS_Products_Config_Db_Storage::class);
		$products_bucket_repository = $this->createMock(OTGS_Products_Bucket_Repository::class);

		$subject = new ExternalProductsUrls(
			$products_config_storage,
			$products_bucket_repository
		);

		$repository_id = 'test_repository';
		$repository_products_url = 'products_url';
		$site_key = 'test_site_key';

		$products_config_storage
			->method('get_repository_products_url')
			->with(
				$this->equalTo($repository_id)
			)
			->willReturn($repository_products_url);


		$this->assertEquals( $repository_products_url, $subject->fetchProductUrl( $repository_id, '', $site_key, '' ) );
	}

	/**
	 * @test
	 */
	public function it_returns_null_from_local_config_if_no_site_key() {
		$products_config_storage = $this->createMock(OTGS_Products_Config_Db_Storage::class);
		$products_bucket_repository = $this->createMock(OTGS_Products_Bucket_Repository::class);

		$subject = new ExternalProductsUrls(
			$products_config_storage,
			$products_bucket_repository
		);

		$repository_id = 'test_repository';
		$repository_products_url = 'products_url';
		$site_key = false;

		$products_config_storage
			->method('get_repository_products_url')
			->with(
				$this->equalTo($repository_id)
			)
			->willReturn($repository_products_url);


		$this->assertNull( $subject->fetchProductUrl( $repository_id, '', $site_key, '' ) );
	}

	/**
	 * @test
	 */
	public function it_fetches_product_url_from_endpoint() {
		$products_config_storage = $this->createMock(OTGS_Products_Config_Db_Storage::class);
		$products_bucket_repository = $this->createMock(OTGS_Products_Bucket_Repository::class);

		$subject = new ExternalProductsUrls(
			$products_config_storage,
			$products_bucket_repository
		);

		$repository_id = 'test_repository';
		$repository_products_url = 'products_url';
		$site_key = 'test_site_key';

		$api_url = 'api_url';

		$products_bucket_repository
			->method('get_products_bucket_url')
			->with(
				$this->equalTo($api_url),
				$this->equalTo($site_key)
			)
			->willReturn($repository_products_url);

		$products_config_storage
			->expects($this->once())
			->method('store_repository_products_url')
			->with(
				$this->equalTo($repository_id),
				$this->equalTo($repository_products_url)
			);

		$this->assertEquals( $repository_products_url, $subject->fetchProductUrl( $repository_id, $api_url, $site_key, '' ) );
	}
}