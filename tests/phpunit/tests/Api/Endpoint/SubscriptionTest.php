<?php

namespace OTGS\Installer\Tests\Api\Endpoint;

use OTGS\Installer\Api\Endpoint\Subscription;
use OTGS\Installer\Api\Exception\InvalidResponseException;
use OTGS\Installer\Api\Exception\InvalidSubscription;
use OTGS\Installer\Api\Exception\InvalidSubscriptionResponseException;
use OTGS\Installer\Api\SiteUrl;
use WP_Mock;

class SubscriptionTest extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_prepares_subscription_request_to_wpml() {
		$repositoryId = 'wpml';
		$subject      = $this->prepareSubject( $repositoryId );

		$siteKey = 'sitekey';
		$source  = 6;

		$expectedParameters = [
			'action'            => 'site_key_validation',
			'site_key'          => 'sitekey',
			'site_url'          => 'https://my-site.com',
			'source'            => 6,
			'installer_version' => '1.0.0',
			'theme'             => 'themeName',
			'site_name'         => 'siteName',
			'wp_version'        => '6.2',
			'phpversion'        => '7.2.34-38+ubuntu20.04.1+deb.sury.org+1',
			'repository_id'     => 'wpml',
			'versions'          => [],
			'using_icl'         => false,
			'wpml_version'      => '',
		];

		$this->assertEquals( $expectedParameters, $subject->prepareRequest( $siteKey, $source ) );
	}

	/**
	 * @test
	 */
	public function it_prepares_subscription_request() {
		$repositoryId = 'toolset';
		$subject      = $this->prepareSubject( $repositoryId );

		$siteKey = 'sitekey';
		$source  = 6;

		$expectedParameters = [
			'action'            => 'site_key_validation',
			'site_key'          => 'sitekey',
			'site_url'          => 'https://my-site.com',
			'source'            => 6,
			'installer_version' => '1.0.0',
			'theme'             => 'themeName',
			'site_name'         => 'siteName',
			'wp_version'        => '6.2',
			'phpversion'        => '7.2.34-38+ubuntu20.04.1+deb.sury.org+1',
			'repository_id'     => 'toolset',
			'versions'          => [],
		];

		$expectedFilteredParameters = [
			'action'            => 'site_key_validation',
			'site_key'          => 'sitekey',
			'site_url'          => 'https://my-site.com',
			'source'            => 6,
			'installer_version' => '1.0.0',
			'theme'             => 'themeName',
			'site_name'         => 'siteName',
			'wp_version'        => '6.2',
			'phpversion'        => '7.2.34-38+ubuntu20.04.1+deb.sury.org+1',
			'repository_id'     => 'toolset',
			'versions'          => [],
			'additional_param'  => 'param_value',

		];

		\WP_Mock::onFilter( 'installer_fetch_subscription_data_request' )
		        ->with( $expectedParameters )
		        ->reply( $expectedFilteredParameters );

		$this->assertEquals( $expectedFilteredParameters, $subject->prepareRequest( $siteKey, $source ) );
	}

	/**
	 * @test
	 */
	public function it_throws_exception_if_response_invalid() {
		$repositoryId = 'wpml';
		$subject      = $this->prepareSubject( $repositoryId );

		$response = $this->getMockBuilder( 'WP_Error' )
		                 ->setMethods( [ 'get_error_message' ] )
		                 ->getMock();

		WP_Mock::userFunction( 'wp_remote_retrieve_body',
			[
				'args'   => [ $response ],
				'return' => null,
			]
		);

		$this->expectException(InvalidResponseException::class);

		$subject->parseResponse($response);
	}

	/**
	 * @test
	 */
	public function it_throws_exception_if_response_not_serialized() {
		$repositoryId = 'wpml';
		$subject      = $this->prepareSubject( $repositoryId );

		$response = $this->getMockBuilder( 'WP_Error' )
		                 ->setMethods( [ 'get_error_message' ] )
		                 ->getMock();
		$responseBody = ['body'];
		$serializedResponseBody = serialize($responseBody);

		WP_Mock::userFunction( 'wp_remote_retrieve_body',
			[
				'args'   => [ $response ],
				'return' => $serializedResponseBody,
			]
		);

		WP_Mock::userFunction( 'is_serialized',
			[
				'args'   => [ $serializedResponseBody ],
				'return' => false,
			]
		);

		$this->expectException(InvalidResponseException::class);
		$subject->parseResponse($response);
	}

	/**
	 * @test
	 */
	public function it_throws_exception_if_response_serialized_but_cannot_unserialize() {
		$repositoryId = 'wpml';
		$subject      = $this->prepareSubject( $repositoryId );

		$response = $this->getMockBuilder( 'WP_Error' )
		                 ->setMethods( [ 'get_error_message' ] )
		                 ->getMock();
		$serializedResponseBody = 'broken_serialized';

		WP_Mock::userFunction( 'wp_remote_retrieve_body',
			[
				'args'   => [ $response ],
				'return' => $serializedResponseBody,
			]
		);

		WP_Mock::userFunction( 'is_serialized',
			[
				'args'   => [ $serializedResponseBody ],
				'return' => true,
			]
		);

		$this->expectException(InvalidResponseException::class);
		$subject->parseResponse($response);
	}

	/**
	 * @test
	 */
	public function it_throws_exception_if_error_in_response() {
		$repositoryId = 'wpml';
		$subject      = $this->prepareSubject( $repositoryId );

		$response = $this->getMockBuilder( 'WP_Error' )
		                 ->setMethods( [ 'get_error_message' ] )
		                 ->getMock();
		$responseBody = new \stdClass();
		$responseBody->error = 'error details';

		$serializedResponseBody = serialize($responseBody);

		WP_Mock::userFunction( 'wp_remote_retrieve_body',
			[
				'args'   => [ $response ],
				'return' => $serializedResponseBody,
			]
		);

		WP_Mock::userFunction( 'is_serialized',
			[
				'args'   => [ $serializedResponseBody ],
				'return' => true,
			]
		);

		$this->expectException(InvalidSubscription::class);
		$subject->parseResponse($response);
	}

	/**
	 * @test
	 */
	public function it_throws_exception_if_missing_attribute_in_response() {
		$repositoryId = 'wpml';
		$subject      = $this->prepareSubject( $repositoryId );

		$response = $this->getMockBuilder( 'WP_Error' )
		                 ->setMethods( [ 'get_error_message' ] )
		                 ->getMock();
		$responseBody = new \stdClass();
		$responseBody->subscription_data = 'subscription_data';

		$serializedResponseBody = serialize($responseBody);

		WP_Mock::userFunction( 'wp_remote_retrieve_body',
			[
				'args'   => [ $response ],
				'return' => $serializedResponseBody,
			]
		);

		WP_Mock::userFunction( 'is_serialized',
			[
				'args'   => [ $serializedResponseBody ],
				'return' => true,
			]
		);

		$this->expectException(InvalidSubscriptionResponseException::class);
		$subject->parseResponse($response);
	}

	/**
	 * @test
	 */
	public function it_returns_parsed_subscription_response() {
		$repositoryId = 'wpml';
		$subject      = $this->prepareSubject( $repositoryId );

		$response = $this->getMockBuilder( 'WP_Error' )
		                 ->setMethods( [ 'get_error_message' ] )
		                 ->getMock();
		$responseBody = new \stdClass();
		$responseBody->subscription_data = 'subscription_data';
		$responseBody->site_key = 'site_key';

		$serializedResponseBody = serialize($responseBody);

		WP_Mock::userFunction( 'wp_remote_retrieve_body',
			[
				'args'   => [ $response ],
				'return' => $serializedResponseBody,
			]
		);

		WP_Mock::userFunction( 'is_serialized',
			[
				'args'   => [ $serializedResponseBody ],
				'return' => true,
			]
		);

		$this->assertEquals($responseBody, $subject->parseResponse($response));
	}

	/**
	 * @param $repositoryId
	 *
	 * @return Subscription
	 */
	public function prepareSubject( $repositoryId ) {
		$siteUrl = $this->createMock( SiteUrl::class );
		$url     = 'https://my-site.com';
		$siteUrl->method( 'get' )->with( $repositoryId )->willReturn( $url );

		$pluginFinder = $this->createMock( \OTGS_Installer_Plugin_Finder::class );
		$pluginFinder->method( 'getLocalPluginVersions' )->willReturn( [] );

		$subject = new Subscription( $repositoryId, $siteUrl, $pluginFinder );


		$themeName    = 'themeName';
		$siteName     = 'siteName';
		$wpVersion    = '6.2';
		$currentTheme = \Mockery::mock( 'WP_Theme' );
		$currentTheme->shouldReceive( 'get' )->with( 'Name' )->andReturn( $themeName );

		\WP_Mock::userFunction( 'wp_get_theme', [ 'return' => $currentTheme ] );
		\WP_Mock::userFunction( 'get_bloginfo', [ 'args' => 'name', 'return' => $siteName ] );
		\WP_Mock::userFunction( 'get_bloginfo', [ 'args' => 'version', 'return' => $wpVersion ] );

		return $subject;
	}
}