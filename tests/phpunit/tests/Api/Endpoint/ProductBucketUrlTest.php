<?php

namespace phpunit\tests\Api\Endpoint;

use OTGS\Installer\Api\Endpoint\ProductBucketUrl;
use OTGS\Installer\Api\Exception\InvalidProductsResponseException;
use WP_Mock;

class ProductBucketUrlTest extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_prepares_product_bucket_url_request() {
		$repositoryId = 'toolset';
		$siteUrl = 'https://my-site.com';
		$subject      = $this->prepareSubject( $repositoryId, $siteUrl );

		$siteKey = 'sitekey';

		$expectedParameters = [
			'action'            => 'product_bucket_url',
			'site_key'          => 'sitekey',
			'site_url'          => $siteUrl,
		];

		$this->assertEquals( $expectedParameters, $subject->prepareRequest( $siteKey ) );
	}

	/**
	 * @test
	 */
	public function it_parses_product_bucket_url_response() {
		$repositoryId = 'toolset';
		$siteUrl = 'https://my-site.com';
		$subject      = $this->prepareSubject( $repositoryId, $siteUrl );

		$response = [
				'response' => [
					'code' => 200,
				]
			];

		$responseBody = json_encode([
			'success' => true,
			'bucket' => [
				'url' => $siteUrl,
			]
		]);

		WP_Mock::userFunction( 'wp_remote_retrieve_body',
			[
				'args'   => [ $response ],
				'return' => $responseBody,
			]
		);

		$this->assertEquals( $siteUrl, $subject->parseResponse( $response ) );
	}

	/**
	 * @test
	 */
	public function it_throws_exception_when_product_bucket_url_response_is_invalid() {
		$repositoryId = 'toolset';
		$siteUrl = 'https://my-site.com';
		$subject      = $this->prepareSubject( $repositoryId, $siteUrl );

		$response = [
			'response' => [
				'code' => 404,
			]
		];

		$this->expectException(InvalidProductsResponseException::class);

		$subject->parseResponse( $response );
	}

	/**
	 * @test
	 */
	public function it_throws_exception_when_product_bucket_url_response_is_false() {
		$repositoryId = 'toolset';
		$siteUrl = 'https://my-site.com';
		$subject      = $this->prepareSubject( $repositoryId, $siteUrl );

		$response = [
			'response' => [
				'code' => 200,
			]
		];

		$responseBody = json_encode([
			'success' => false,
			'bucket' => [
				'url' => 'https://my-site.com',
			]
		]);

		WP_Mock::userFunction( 'wp_remote_retrieve_body',
			[
				'args'   => [ $response ],
				'return' => $responseBody,
			]
		);

		$this->expectException(InvalidProductsResponseException::class);
		$subject->parseResponse( $response );
	}

	private function prepareSubject( $repositoryId, $siteUrl ) {
		$siteUrlMock = $this->getMockBuilder( 'OTGS\Installer\Api\SiteUrl' )
		                ->disableOriginalConstructor()
		                ->getMock();

		$siteUrlMock->method( 'get' )
		        ->willReturn( $siteUrl );

		return new ProductBucketUrl( $repositoryId, $siteUrlMock);
	}
}