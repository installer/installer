<?php

namespace OTGS\Installer\Tests\Api;

use OTGS\Installer\Api\Client\Client;
use OTGS\Installer\Api\Endpoint\ProductBucketUrl;
use OTGS\Installer\Api\Endpoint\Subscription;
use OTGS\Installer\Api\Exception\ClientException;
use OTGS\Installer\Api\InstallerApiClient;
use OTGS_Installer_Logger_Storage;
use WP_Mock;

class InstallerApiClientTest extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_sends_fetch_subscription_request() {
		$client                   = $this->createMock( Client::class );
		$loggerStorage            = $this->createMock( OTGS_Installer_Logger_Storage::class );
		$subscriptionEndpoint     = $this->createMock( Subscription::class );
		$productBucketUrlEndpoint = $this->createMock( ProductBucketUrl::class );

		$subject = new InstallerApiClient( $loggerStorage, $client, $subscriptionEndpoint, $productBucketUrlEndpoint );

		$request = [ 'body' => 'bodyRequest' ];
		$siteKey = 'sitekey';
		$source  = 6;

		$response = [ 'response' ];

		$responseObj = (object) [
			'subscription_data' => [ 'data' ],
			'site_key'          => [ 'data' ],
		];

		$subscriptionEndpoint->method( 'prepareRequest' )->with( $siteKey, $source )->willReturn( $request );
		$subscriptionEndpoint->method( 'parseResponse' )->with( $response )->willReturn( $responseObj );

		$client->method( 'post' )->with( $request )->willReturn( $response );

		$result = $subject->fetchSubscription( $siteKey, $source );
		$this->assertEquals( $result, $responseObj );
	}

	/**
	 * @test
	 */
	public function it_sends_fetch_product_url_request() {
		$client                   = $this->createMock( Client::class );
		$loggerStorage            = $this->createMock( OTGS_Installer_Logger_Storage::class );
		$subscriptionEndpoint     = $this->createMock( Subscription::class );
		$productBucketUrlEndpoint = $this->createMock( ProductBucketUrl::class );

		$subject = new InstallerApiClient( $loggerStorage, $client, $subscriptionEndpoint, $productBucketUrlEndpoint );

		$request = [ 'body' => 'bodyRequest' ];
		$siteKey = 'sitekey';

		$response = [ 'response' ];

		$responseBody = 'https://my-site.com';

		$productBucketUrlEndpoint->method( 'prepareRequest' )->with( $siteKey )->willReturn( $request );
		$productBucketUrlEndpoint->method( 'parseResponse' )->with( $response )->willReturn( $responseBody );

		$client->method( 'post' )->with( $request )->willReturn( $response );


		$result = $subject->fetchProductUrl( $siteKey );
		$this->assertEquals( $result, $responseBody );
	}

	/**
	 * @test
	 */
	public function it_throws_exception_on_fetch_subscription_request_error() {
		$client                   = $this->createMock( Client::class );
		$loggerStorage            = $this->createMock( OTGS_Installer_Logger_Storage::class );
		$subscriptionEndpoint     = $this->createMock( Subscription::class );
		$productBucketUrlEndpoint = $this->createMock( ProductBucketUrl::class );

		$subject = new InstallerApiClient( $loggerStorage, $client, $subscriptionEndpoint, $productBucketUrlEndpoint );

		$request = [ 'body' => 'bodyRequest' ];
		$siteKey = 'sitekey';
		$source  = 6;

		$subscriptionEndpoint->method( 'prepareRequest' )->with( $siteKey, $source )->willReturn( $request );
		$client->method( 'post' )->with( $request )->willThrowException( new ClientException( "details" ) );

		$this->expectException( \OTGS_Installer_Fetch_Subscription_Exception::class );
		$subject->fetchSubscription( $siteKey, $source );
	}

	/**
	 * @test
	 */
	public function it_throws_exception_on_fetch_product_url_request_error() {
		$client                   = $this->createMock( Client::class );
		$loggerStorage            = $this->createMock( OTGS_Installer_Logger_Storage::class );
		$subscriptionEndpoint     = $this->createMock( Subscription::class );
		$productBucketUrlEndpoint = $this->createMock( ProductBucketUrl::class );

		$subject = new InstallerApiClient( $loggerStorage, $client, $subscriptionEndpoint, $productBucketUrlEndpoint );

		$request = [ 'body' => 'bodyRequest' ];
		$siteKey = 'sitekey';

		$productBucketUrlEndpoint->method( 'prepareRequest' )->with( $siteKey )->willReturn( $request );
		$client->method( 'post' )->with( $request )->willThrowException( new ClientException( "details" ) );

		$this->assertFalse( $subject->fetchProductUrl( $siteKey ) );
	}
}