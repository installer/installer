<?php

namespace OTGS\Installer\Tests\Api;

use OTGS\Installer\Api\SiteUrl;

class SiteUrlTest extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_gets_site_url() {
		$siteUrl = 'https://my-site.com';
		$subject = new SiteUrl( [ 'repositories' => [] ] );

		\WP_Mock::userFunction( 'get_site_url', [ 'return' => $siteUrl ] );

		$this->assertEquals( $siteUrl, $subject->get() );
	}

	/**
	 * @test
	 */
	public function it_gets_filtered_site_url() {
		$siteUrl         = 'https://my-site.com';
		$filteredSiteUrl = 'https://my-site-filtered.com';
		$subject         = new SiteUrl( [ 'repositories' => [] ] );

		\WP_Mock::onFilter( 'otgs_installer_site_url' )
		        ->with( $siteUrl )
		        ->reply( $filteredSiteUrl );

		\WP_Mock::userFunction( 'get_site_url', [ 'return' => $siteUrl ] );

		$this->assertEquals( $filteredSiteUrl, $subject->get() );
	}

	/**
	 * @test
	 */
	public function it_gets_proper_site_url_on_multisite() {
		global $current_site;
		$current_site = new \stdClass();
		$current_site->blog_id = 123;
		$siteUrl       = 'https://my-site.com';
		$multisiteSiteUrl       = 'https://my-site-multisite.com';
		$subject       = new SiteUrl( [ 'repositories' => [] ] );
		$repository_id = 'wpml';

		\WP_Mock::userFunction( 'get_site_url', [ 'return_in_order' => [$siteUrl, $multisiteSiteUrl] ] );

		\WP_Mock::userFunction( 'is_multisite', [ 'return' => true ] );
		\WP_Mock::userFunction( 'get_site_option', [ 'return' => [] ] );
		\WP_Mock::userFunction( 'maybe_unserialize', [ 'return' => [ $repository_id => [] ] ] );

		$this->assertEquals( $multisiteSiteUrl, $subject->get( $repository_id ) );
	}
}