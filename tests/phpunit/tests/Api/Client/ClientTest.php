<?php

namespace OTGS\Installer\Tests\Api\Client;

use OTGS\Installer\Api\Client\Client;
use OTGS\Installer\Api\Exception\ClientException;
use WP_Mock;

class ClientTest extends \OTGS_TestCase {
	/**
	 * @test
	 */
	public function it_sends_post_request() {
		$httpClient = \Mockery::mock( 'WP_Http' );
		$url     = 'example.url';
		$subject    = new Client( $httpClient, $url );

		$request = [ 'body' => 'bodyRequest' ];


		$response = [ 'response' ];

		WP_Mock::userFunction( 'is_wp_error',
			[
				'args'   => [ $response ],
				'return' => false,
			]
		);

		$httpClient->expects( 'post' )->with( $url, [
			'timeout' => Client::TIMEOUT,
			'body'    => 'bodyRequest'
		] )->andReturn( $response );


		$result = $subject->post( $request['body'] );

		$this->assertEquals( $result, $response );
	}

	/**
	 * @test
	 */
	public function it_throws_exception_on_error() {
		$httpClient = \Mockery::mock( 'WP_Http' );
		$url     = 'example.url';
		$subject    = new Client( $httpClient, $url );

		$request = [ 'body' => 'bodyRequest' ];

		$response = $this->getMockBuilder( 'WP_Error' )
		                 ->setMethods( [ 'get_error_message' ] )
		                 ->getMock();


		WP_Mock::userFunction( 'is_wp_error',
			[
				'args'   => [ $response ],
				'return' => true,
			]
		);

		$httpClient->expects( 'post' )->with( $url, [
			'timeout' => Client::TIMEOUT,
			'body'    => 'bodyRequest'
		] )->andReturn( $response );

		$this->expectException( ClientException::class );
		$subject->post( $request['body'] );
	}
}