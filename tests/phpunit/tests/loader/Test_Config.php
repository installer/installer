<?php

namespace OTGS\Installer\Loader;

class Test_Config extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_returns_unchanged_if_nothing_to_merge() {

		$delegate = [ 'bootfile' => 'anything.php' ];

		$wpInstallerInstances = [ $delegate ];
		$this->assertEquals( $delegate, Config::merge( $delegate, $wpInstallerInstances ) );
	}

	/**
	 * @test
	 */
	public function it_adds_nag_from_other() {
		$delegate = [ 'bootfile' => 'anything.php' ];
		$nags     = [ 'some', 'nags' ];
		$other    = [ 'bootfile' => 'something_else.php', 'args' => [ 'site_key_nags' => $nags ] ];

		$wpInstallerInstances = [ $delegate, $other ];
		$merged               = Config::merge( $delegate, $wpInstallerInstances );

		$this->assertEquals( $nags, $merged['args']['site_key_nags'] );
	}

	/**
	 * @test
	 */
	public function it_mergs_nag_from_other() {
		$delegate_nags = [ 'my', 'nags' ];
		$delegate      = [ 'bootfile' => 'anything.php', 'args' => [ 'site_key_nags' => $delegate_nags ] ];
		$other_nags    = [ 'some', 'nags' ];
		$other         = [ 'bootfile' => 'something_else.php', 'args' => [ 'site_key_nags' => $other_nags ] ];

		$wpInstallerInstances = [ $delegate, $other ];
		$merged               = Config::merge( $delegate, $wpInstallerInstances );

		$this->assertEquals( array_merge_recursive( $delegate_nags, $other_nags ), $merged['args']['site_key_nags'] );
	}

}
