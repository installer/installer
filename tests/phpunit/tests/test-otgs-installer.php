<?php

class Test_OTGS_Installer extends OTGS_TestCase {

	public function setUp() {
		parent::setUp();

		WP_Mock::userFunction( 'is_admin', array() );
	}

	/**
	 * @test
	 */
	public function it_returns_warning_when_user_has_no_capability_to_show_products() {
		WP_Mock::userFunction( 'get_option', [ 'args' => [ 'wp_installer_settings' ] ] );
		WP_Mock::wpFunction( 'is_multisite', array(
			'return' => false,
		) );

		\WP_Mock::userFunction( 'update_option', [] );

		\WP_Mock::userFunction( 'get_site_url', array(
			'return' => 'http://dev.otgs',
		) );

		\WP_Mock::userFunction( 'current_user_can', array(
			'return' => false,
		) );

		\WP_Mock::userFunction( 'wp_die', array(
			'times' => 1,
			'args'  => array(
				'Sorry, you are not allowed to manage Installer for this site.'
			)
		) );

		$subject = new WP_Installer();
		$subject->show_products( [] );
	}

	/**
	 * @param array $packages
	 * 
	 * @return array
	 */
	private function addPackageData( $packages ) {
		$packages[0]['id']          = 'test_product_package_1';
		$packages[0]['image_url']   = 'https://test.com/testproduct.jpg';
		$packages[0]['name']        = 'Test Product Package';
		$packages[0]['description'] = 'Test Product Package Description';

		return $packages;
	}

	public function shouldUseApiData() {
		return [
			[ false ],
			[ true ],
		];
	}

	/**
	 * @test
	 * @dataProvider shouldUseApiData
	 */
	public function it_creates_pricing_labels_with_default_currency_and_fixed_price( $shouldUseApiData = false ) {
		WP_Mock::wpFunction( 'add_query_arg', array(
			'return' => function( $url ) {
				return $url;
			},
		) );

		$expectedLabelWithoutDiscount = 'Test Product - &#36;199 (USD)';
		$expectedLabelWithDiscount    = 'Test Product2 - &#36;149 &nbsp;&nbsp;<del>&#36;199</del> (USD)';

		if ( $shouldUseApiData ) {
			$expectedLabelWithoutDiscount = 'Test Product - &#x20AC;199 (EUR)';
			$expectedLabelWithDiscount    = 'Test Product2 - &#x20AC;149 &nbsp;&nbsp;<del>&#x20AC;199</del> (EUR)';
		}

		$repository       = [
			'data' => [],
		];
		$packages         = $this->addPackageData( [
			[
				'products'  => [
					[
						'call2action'       => 'Test Product',
						'price'             => 199,
						'url'               => 'https://test.com/testproduct',
						'subscription_type' => '',
					],
					[
						'call2action'       => 'Test Product2',
						'price'             => 199,
						'url'               => 'https://test.com/testproduct2',
						'subscription_type' => '',
						'price_disc'        => 149,
					],
				],
			],
		] );
		$subscriptionType = null;
		$expired          = false;
		$upgradeOptions   = null;
		$repositoryId     = 'wpml';

		if ( $shouldUseApiData ) {
			$repository['data']['shop_currency_symbol'] = '&#x20AC;';
			$repository['data']['shop_currency']        = 'EUR';
		}

		$subject = new WP_Installer();
		$res     = $subject->getRenderProductPackagesData( $repository, $packages, $subscriptionType, $expired, $upgradeOptions, $repositoryId )[0]['products'];

		$this->assertEquals( $expectedLabelWithoutDiscount, $res[0]['label'] );
		$this->assertEquals( $expectedLabelWithDiscount, $res[1]['label'] );
	}

	/**
	 * @test
	 * @dataProvider shouldUseApiData
	 */
	public function it_creates_pricing_labels_with_default_currency_and_subscription_renewal( $shouldUseApiData = false ) {
		$expectedLabel = 'Test Renewal - &#36;39 (USD)';

		if ( $shouldUseApiData ) {
			$expectedLabel = 'Test Renewal - &#x20AC;39 (EUR)';
		}

		$repository       = [
			'data' => [],
		];
		$packages         = $this->addPackageData( [
			[
				'products'  => [
					[
						'call2action'       => 'Test Product',
						'price'             => 199,
						'url'               => 'https://test.com/testproduct',
						'subscription_type' => 10000,
						'plugins'           => [],
						'renewals'          => [
							[
								'url'         => 'https://test.com/testproduct/renew',
								'call2action' => 'Test Renewal',
								'price'       => 39,
							],
						],
					],
				],
			],
		] );
		$subscriptionType = 10000;
		$expired          = false;
		$upgradeOptions   = null;
		$repositoryId     = 'wpml';

		if ( $shouldUseApiData ) {
			$repository['data']['shop_currency_symbol'] = '&#x20AC;';
			$repository['data']['shop_currency']        = 'EUR';
		}

		$subject = new WP_Installer();
		$res     = $subject->getRenderProductPackagesData( $repository, $packages, $subscriptionType, $expired, $upgradeOptions, $repositoryId )[0]['products'];

		$this->assertEquals( $expectedLabel, $res[0]['label'] );
	}

	/**
	 * @test
	 * @dataProvider shouldUseApiData
	 */
	public function it_creates_pricing_labels_with_default_currency_and_subscription_upgrade( $shouldUseApiData = false ) {
		$expectedLabelWithoutDiscount = 'Test Product Upgrade - &#36;99 (USD)';
		$expectedLabelWithDiscount    = 'Test Product Upgrade With Disc - &#36;149 &nbsp;&nbsp;<del>&#36;99</del> (USD)';

		if ( $shouldUseApiData ) {
			$expectedLabelWithoutDiscount = 'Test Product Upgrade - &#x20AC;99 (EUR)';
			$expectedLabelWithDiscount    = 'Test Product Upgrade With Disc - &#x20AC;149 &nbsp;&nbsp;<del>&#x20AC;99</del> (EUR)';
		}

		$repository       = [
			'data' => [],
		];
		$packages         = $this->addPackageData( [
			[
				'products'  => [
					[
						'call2action'       => 'Test Product',
						'price'             => 199,
						'url'               => 'https://test.com/testproduct',
						'subscription_type' => 20000,
						'plugins'           => [],
						'renewals'          => [],
					],
				],
			],
		] );
		$subscriptionType = 20000;
		$expired          = false;
		$upgradeOptions   = [
			20000 => [
				20000 => [
					'call2action'       => 'Test Product Upgrade',
					'price'             => 99,
					'url'               => 'https://test.com/testproductupgrade',
					'subscription_type' => 20000,
					'plugins'           => [],
				],
			],
		];
		$repositoryId     = 'wpml';

		if ( $shouldUseApiData ) {
			$repository['data']['shop_currency_symbol'] = '&#x20AC;';
			$repository['data']['shop_currency']        = 'EUR';
		}

		$subject = new WP_Installer();
		$res     = $subject->getRenderProductPackagesData( $repository, $packages, $subscriptionType, $expired, $upgradeOptions, $repositoryId )[0]['products'];

		$upgradeOptions   = [
			20000 => [
				20000 => [
					'call2action'       => 'Test Product Upgrade With Disc',
					'price'             => 99,
					'price_disc'        => 149,
					'url'               => 'https://test.com/testproductupgrade',
					'subscription_type' => 20000,
					'plugins'           => [],
				],
			],
		];
		$resWithDiscount = $subject->getRenderProductPackagesData( $repository, $packages, $subscriptionType, $expired, $upgradeOptions, $repositoryId )[0]['products'];

		$this->assertEquals( $expectedLabelWithoutDiscount, $res[0]['label'] );
		$this->assertEquals( $expectedLabelWithDiscount, $resWithDiscount[0]['label'] );
	}
}
