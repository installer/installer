<?php

namespace OTGS\Installer\AdminNotices\Notices;

use OTGS\Installer\AdminNotices\Store;
use OTGS\Installer\AdminNotices\storeMock;
use OTGS\Installer\Recommendations\Storage;

/**
 * @package OTGS\Installer\AdminNotices
 * @group admin-notices
 */
class Test_Recommendation extends \OTGS_TestCase {

	private $storeData = [];
	public function setUp() {
		parent::setUp();

		\WP_Mock::userFunction( 'get_option',
			[
				'return' => function( $key, $default ) {
					return isset( $this->storeData[$key] ) ? $this->storeData[$key] : $default;
				}
			]
		);
		\WP_Mock::userFunction( 'update_option',
			[
				'return' => function( $key, $value , $autoload ) {
					$this->storeData[$key] = $value;
				}
			]
		);
	}

	/**
	 * @test
	 */
	public function it_gets_no_message_when_there_are_not_any() {
		$this->assertEquals( [], Recommendation::getCurrentNotices( [] ) );
	}

	/**
	 * @test
	 */
	public function it_displays_on_correct_screens() {

		$initialScreens = [ 'repo' => [ 'initial' => [] ] ];

		$expectedScreens = array_merge_recursive( $initialScreens, [
			'repo' => [
				'wpml'    => [
					Recommendation::PLUGIN_ACTIVATED => [ 'screens' => [ 'plugins' ] ],
				],
			]
		] );

		$this->assertEquals( $expectedScreens, Recommendation::screens( $initialScreens ) );
	}

	/**
	 * @test
	 */
	public function it_has_message_texts() {
		$initialTexts = [ 'repo' => [ 'initial' => [] ] ];

		$expectedTexts = array_merge( $initialTexts, [
			'repo' => [
				'wpml'    => [
					Recommendation::PLUGIN_ACTIVATED => WPMLTexts::class . '::pluginActivatedRecommendation',
				],
				'initial' => [],
			]
		] );

		$this->assertEquals( $expectedTexts, Recommendation::texts( $initialTexts ) );
	}

	/**
	 * @test
	 */
	public function it_returns_config_with_screens() {
		$initialConfig = [ 'repo' => [ 'initial' => [] ] ];
		$config        = Recommendation::config( $initialConfig );

		$messages = [ Recommendation::PLUGIN_ACTIVATED ];
		$repos    = [ 'wpml' ];
		$types    = [ 'screens' ];
		foreach ( $repos as $repo ) {
			foreach ( $messages as $message ) {
				foreach ( $types as $type ) {
					$this->assertTrue( is_array( $config['repo'][ $repo ][ $message ][ $type ] ) );
				}
			}
		}
	}
}
