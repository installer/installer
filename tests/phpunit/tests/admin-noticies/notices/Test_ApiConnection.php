<?php

namespace OTGS\Installer\AdminNotices\Notices;

use OTGS\Installer\AdminNotices\Store;
use OTGS\Installer\AdminNotices\storeMock;

/**
 * Class Test_Account
 * @package OTGS\Installer\AdminNotices
 * @group admin-notices
 */
class Test_ApiConnection extends \OTGS_TestCase {

	use storeMock;

	/**
	 * @test
	 */
	public function it_gets_no_message_when_there_are_not_any() {
		$installer = \Mockery::mock( \WP_Installer::class );
		$installer->shouldReceive( 'getRepositories' )->andReturn( [] );

		$this->assertEquals( [], ApiConnection::getCurrentNotices( $installer, [] ) );
	}

	/**
	 * @test
	 */
	public function it_gets_connection_issues_message() {
		$installer = \Mockery::mock( \WP_Installer::class );
		$repo      = 'toolset';
		$installer->shouldReceive( 'getRepositories' )->andReturn(
			[ [ 'repository_id' => $repo ] ]
		);
		$installer->shouldReceive( 'shouldDisplayConnectionIssueMessage' )->with( $repo )->andReturn( true );

		self::initializeOldInstall( $repo );

		$expected = [ 'repo' => [ $repo => [ ApiConnection::CONNECTION_ISSUES ] ] ];

		$this->assertEquals( $expected, ApiConnection::getCurrentNotices( $installer, [] ) );
	}

	/**
	 * @test
	 */
	public function it_displays_on_correct_screens() {

		$initialScreens = [ 'repo' => [ 'initial' => [] ] ];

		$expectedScreens = array_merge_recursive( $initialScreens, [
			'repo' => [
				'wpml'    => [
					ApiConnection::CONNECTION_ISSUES => [ 'screens' => [ 'plugins', 'plugin-install' ] ],
				],
				'toolset' => [
					ApiConnection::CONNECTION_ISSUES       => [ 'screens' => [ 'plugins', 'plugin-install' ] ],
				],
			]
		] );

		$this->assertEquals( $expectedScreens, ApiConnection::screens( $initialScreens ) );
	}

	/**
	 * @test
	 */
	public function it_displays_on_correct_pages() {
		if ( ! defined( 'WPML_PLUGIN_FOLDER' ) ) {
			define( 'WPML_PLUGIN_FOLDER', 'sitepress-multilingual-cms' );
		}

		$initialPages = [ 'repo' => [ 'initial' => [] ] ];

		$wpmlPages     = [
			'pages' =>
				[
					WPML_PLUGIN_FOLDER . '/menu/languages.php',
					WPML_PLUGIN_FOLDER . '/menu/theme-localization.php',
					WPML_PLUGIN_FOLDER . '/menu/settings.php',
					WPML_PLUGIN_FOLDER . '/menu/support.php',
				]
		];
		$toolsetPages  = [
			'pages' => [
				'toolset-dashboard',
			],
		];
		$expectedPages = array_merge_recursive( $initialPages, [
			'repo' => [
				'wpml'    => [
					ApiConnection::CONNECTION_ISSUES => $wpmlPages,
				],
				'toolset' => [
					ApiConnection::CONNECTION_ISSUES => $toolsetPages,
				],
			],
		] );

		$this->assertEquals( $expectedPages, ApiConnection::pages( $initialPages ) );
	}

	/**
	 * @test
	 */
	public function it_has_message_texts() {
		$initialTexts = [ 'repo' => [ 'initial' => [] ] ];

		$expectedTexts = array_merge( $initialTexts, [
			'repo' => [
				'wpml'    => [
					ApiConnection::CONNECTION_ISSUES => WPMLTexts::class . '::connectionIssues',
				],
				'toolset' => [
					ApiConnection::CONNECTION_ISSUES => ToolsetTexts::class . '::connectionIssues',
				],
				'initial' => [],
			]
		] );

		$this->assertEquals( $expectedTexts, ApiConnection::texts( $initialTexts ) );
	}

	/**
	 * @test
	 */
	public function it_returns_config_with_screens_and_pages() {
		$initialConfig = [ 'repo' => [ 'initial' => [] ] ];
		$config        = ApiConnection::config( $initialConfig );

		$messages = [ ApiConnection::CONNECTION_ISSUES ];
		$repos    = [ 'wpml', 'toolset' ];
		$types    = [ 'pages', 'screens' ];
		foreach ( $repos as $repo ) {
			foreach ( $messages as $message ) {
				foreach ( $types as $type ) {
					$this->assertTrue( is_array( $config['repo'][ $repo ][ $message ][ $type ] ) );
				}
			}
		}
	}

	/**
	 * @test
	 */
	public function it_renders_texts() {
		$admin_url = 'http://something/wp-admin/admin.php?page=otgs-installer-support';

		\WP_Mock::userFunction( 'admin_url', array(
			'return' => $admin_url,
			'args'   => '/admin.php?page=otgs-installer-support',
		) );

		$text = WPMLTexts::connectionIssues();
		$this->assertHasTag( 'h2', $text );
		$this->assertHasTag( 'p', $text );
		$this->assertCanNotDismiss( $text );

		$text = ToolsetTexts::connectionIssues();
		$this->assertHasTag( 'h2', $text );
		$this->assertHasTag( 'p', $text );
		$this->assertCanNotDismiss( $text );
	}

	private function assertHasTag( $tag, $text ) {
		$this->assertContains( "<$tag", $text );
		$this->assertContains( "</$tag>", $text );
	}

	private static function initializeOldInstall( $repo ) {
		$store = new Store();
		$slightlyMoreThanOneWeek = time() - WEEK_IN_SECONDS - 10;
		$store->save( Account::GET_FIRST_INSTALL_TIME, [ $repo => $slightlyMoreThanOneWeek ] );
	}

	private function assertCanNotDismiss( string $text ) {
		$this->assertNotRegExp('/<div.*?class=".*?is-dismissible.*?"/', $text);
	}
}
