<?php

namespace OTGS\Installer\AdminNotices\Notices;

use OTGS\Installer\AdminNotices\Store;
use tad\FunctionMocker\FunctionMocker;
use OTGS\Installer\AdminNotices\storeMock;

/**
 * @package OTGS\Installer\AdminNotices
 * @group admin-notices
 */
class Test_Hooks extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_adds_hooks() {
		\WP_Mock::expectFilterAdded( 'otgs_installer_admin_notices_config', [Account::class, 'config'] );
		\WP_Mock::expectFilterAdded( 'otgs_installer_admin_notices_texts', [Account::class, 'texts'] );

		Hooks::addHooks(Account::class, \Mockery::mock( \WP_Installer::class ));

		\WP_Mock::expectFilterAdded( 'otgs_installer_admin_notices_config', [ApiConnection::class, 'config'] );
		\WP_Mock::expectFilterAdded( 'otgs_installer_admin_notices_texts', [ApiConnection::class, 'texts'] );

		Hooks::addHooks(ApiConnection::class, \Mockery::mock( \WP_Installer::class ));
	}

}
