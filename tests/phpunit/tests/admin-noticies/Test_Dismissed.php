<?php

namespace OTGS\Installer\AdminNotices;

/**
 * Class Test_Dismissed
 * @package OTGS\Installer\AdminNotices
 * @group admin-notices
 */
class Test_Dismissed extends \OTGS_TestCase {

	private $storeData = [];
	public function setUp() {
		parent::setUp();

		\WP_Mock::userFunction( 'get_option',
			[
				'return' => function( $key, $default ) {
					return isset( $this->storeData[$key] ) ? $this->storeData[$key] : $default;
				}
			]
		);
		\WP_Mock::userFunction( 'update_option',
			[
				'return' => function( $key, $value , $autoload ) {
					$this->storeData[$key] = $value;
				}
			]
		);
	}

	/**
	 * @return array[]
	 */
	function dp_it_dismisses_by_plugin_notice() {
		return [
			'Activate WCML plugin having WC notice' => [
				'definedNotices' => [
					'repo' => [
						'wpml' => [
							'plugin-activated' => [
								'woocommerce' => [
									'glue_plugin_slug' => 'woocommerce-multilingual',
									'notice_type'      => 'new',
									'notice'           => 'test',
								],
							],
						],
					],
				],
				'activatedPlugin' => 'woocommerce-multilingual/wpml-woocommerce.php',
				'expectedDismissedRepo' => 'wpml',
				'expectedDismissedPlugin' => 'woocommerce',
			],
			'Activate other plugin having WC notice' => [
				'definedNotices' => [
					'repo' => [
						'wpml' => [
							'plugin-activated' => [
								'woocommerce' => [
									'glue_plugin_slug' => 'woocommerce-multilingual',
									'notice_type'      => 'new',
									'notice'           => 'test',
								],
							],
						],
					],
				],
				'activatedPlugin' => 'toolset-blocks/toolset-blocks.php',
				'expectedDismissedRepo' => false,
				'expectedDismissedPlugin' => false,
			]
		];
	}

	/**
	 * @test
	 * @dataProvider dp_it_dismisses_by_plugin_notice
	 *
	 * @param array $definedNotices
	 * @param string $activatedPlugin
	 * @param string|false $expectedDismissedRepo
	 * @param string|false $expectedDismissedPlugin
	 * @return void
	 */
	public function it_dismisses_activated_plugin_notice( $definedNotices, $activatedPlugin, $expectedDismissedRepo, $expectedDismissedPlugin) {

		if ( $expectedDismissedRepo ) {
			\WP_Mock::onFilter( 'otgs_installer_admin_notices_dismissions' )
				->with( [] )
				->reply(
					[ 'plugin-activated' => function() use ($definedNotices, $expectedDismissedRepo, $expectedDismissedPlugin ) {
						$existing = $definedNotices;
						$existing['repo'][ $expectedDismissedRepo ][ 'plugin-activated' ][ $expectedDismissedPlugin ] = time();
						return $existing;
					}]
				);
		}

		\WP_Mock::onFilter( 'otgs_installer_admin_notices')->with([])->reply( $definedNotices );

		$store = new Store();
		$store->save( 'dismissed', [] );

		Dismissed::dismissNoticeOnPluginActivation( $activatedPlugin, false );

		$dismissed = $store->get( 'dismissed', [] );

		$this->assertEquals( !! $expectedDismissedRepo, isset( $dismissed['repo'][ $expectedDismissedRepo ][ 'plugin-activated' ][$expectedDismissedPlugin] ) );

	}

	/**
	 * @test
	 */
	public function it_dismisses_notice() {
		$repo                = 'wpml';
		$_POST['repository'] = $repo;
		$noticeType           = 'new';
		$_POST['noticeType']  = $noticeType;

		$existingNoticeType = 'original';
		$existing          = [ 'repo' => [ $repo => [ $existingNoticeType => time() ] ] ];

		$store = new Store();
		$store->save( 'dismissed', $existing );

		\WP_Mock::userFunction( 'wp_send_json_success', [ 'times' => 1 ] );

		\WP_Mock::onFilter( 'otgs_installer_admin_notices_dismissions' )
		        ->with( [] )
		        ->reply( [$noticeType => function() use ($existing, $repo, $noticeType) {
			        $existing['repo'][ $repo ][ $noticeType ] = time();

			        return $existing;
		        }] );

		Dismissed::dismissNotice();

		$dismissed = $store->get( 'dismissed', [] );
		$this->assertTrue( isset( $dismissed['repo'][ $repo ][ $noticeType ] ) );
		$this->assertTrue( isset( $dismissed['repo'][ $repo ][ $existingNoticeType ] ) );

		unset( $_POST['repository'], $_POST['noticeId'] );
	}

}
