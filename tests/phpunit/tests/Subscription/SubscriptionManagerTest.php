<?php

namespace OTGS\Installer\Tests\Subscription;

use OTGS\Installer\Api\Exception\InvalidProductBucketUrl;
use OTGS\Installer\Api\InstallerApiClient;
use OTGS\Installer\Subscription\SubscriptionManager;

class SubscriptionManagerTest extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_fetches_subscription() {
		$siteKey          = 'site-key';
		$source           = 'source';
		$subscriptionData = 'subscription-data';
		$repositoryId     = 'repository-id';

		$installerApiClient    = $this->createMock( InstallerApiClient::class );
		$productsConfigStorage = $this->createMock( \OTGS_Products_Config_Db_Storage::class );

		$installerApiClient->expects( $this->once() )
		                   ->method( 'fetchSubscription' )
		                   ->with( $siteKey, $source )
		                   ->willReturn( (object) [
			                   'subscription_data' => $subscriptionData,
			                   'site_key'          => $siteKey,
			                   'bucket_version'    => 3,
		                   ] );

		$productsConfigStorage->expects( $this->once() )
		                      ->method( 'get_repository_product_version' )
		                      ->with( $repositoryId )
		                      ->willReturn( 3 );

		$productsConfigStorage->expects( $this->never() )
		                      ->method( 'store_repository_products_url' );

		$subscriptionManager = new SubscriptionManager( $repositoryId, $installerApiClient, $productsConfigStorage );

		$this->assertEquals( [ $subscriptionData, $siteKey ], $subscriptionManager->fetch( $siteKey, $source ) );
	}

	/**
	 * @test
	 */
	public function it_fetches_subscription_and_product_url_if_version_lower() {
		$siteKey              = 'site-key';
		$source               = 'source';
		$subscriptionData     = 'subscription-data';
		$repositoryId         = 'repository-id';
		$productUrl           = 'product-url';
		$newProductUrlVersion = 5;

		$installerApiClient    = $this->createMock( InstallerApiClient::class );
		$productsConfigStorage = $this->createMock( \OTGS_Products_Config_Db_Storage::class );

		$installerApiClient->expects( $this->once() )
		                   ->method( 'fetchSubscription' )
		                   ->with( $siteKey, $source )
		                   ->willReturn( (object) [
			                   'subscription_data' => $subscriptionData,
			                   'site_key'          => $siteKey,
			                   'bucket_version'    => $newProductUrlVersion,
		                   ] );

		$installerApiClient->expects( $this->once() )
		                   ->method( 'fetchProductUrl' )
		                   ->with( $siteKey )
		                   ->willReturn( $productUrl );

		$productsConfigStorage->expects( $this->once() )
		                      ->method( 'get_repository_product_version' )
		                      ->with( $repositoryId )
		                      ->willReturn( 3 );

		$productsConfigStorage->expects( $this->once() )
		                      ->method( 'store_repository_products_url' )
		                      ->with( $repositoryId, $productUrl );

		$productsConfigStorage->expects( $this->once() )
		                      ->method( 'update_repository_product_version' )
		                      ->with( $repositoryId, $newProductUrlVersion );

		$subscriptionManager = new SubscriptionManager( $repositoryId, $installerApiClient, $productsConfigStorage );

		$this->assertEquals( [ $subscriptionData, $siteKey ], $subscriptionManager->fetch( $siteKey, $source ) );
	}

	/**
	 * @test
	 */
	public function it_throws_error_on_fetch_subscription_failure() {
		$siteKey      = 'site-key';
		$source       = 'source';
		$repositoryId = 'repository-id';


		$installerApiClient    = $this->createMock( InstallerApiClient::class );
		$productsConfigStorage = $this->createMock( \OTGS_Products_Config_Db_Storage::class );

		$installerApiClient->expects( $this->once() )
		                   ->method( 'fetchSubscription' )
		                   ->with( $siteKey, $source )
		                   ->willThrowException( new \OTGS_Installer_Fetch_Subscription_Exception( 'error' ) );

		$subscriptionManager = new SubscriptionManager( $repositoryId, $installerApiClient, $productsConfigStorage );

		$this->expectException( \OTGS_Installer_Fetch_Subscription_Exception::class );

		$subscriptionManager->fetch( $siteKey, $source );
	}

	/**
	 * @test
	 */
	public function it_throws_error_on_fetch_product_url_failure() {
		$siteKey              = 'site-key';
		$source               = 'source';
		$repositoryId         = 'repository-id';
		$subscriptionData     = 'subscription-data';
		$productUrl           = 'product-url';
		$newProductUrlVersion = 5;

		$installerApiClient    = $this->createMock( InstallerApiClient::class );
		$productsConfigStorage = $this->createMock( \OTGS_Products_Config_Db_Storage::class );

		$installerApiClient->expects( $this->once() )
		                   ->method( 'fetchSubscription' )
		                   ->with( $siteKey, $source )
		                   ->willReturn( (object) [
			                   'subscription_data' => $subscriptionData,
			                   'site_key'          => $siteKey,
			                   'bucket_version'    => $newProductUrlVersion,
		                   ] );

		$installerApiClient->expects( $this->once() )
		                   ->method( 'fetchProductUrl' )
		                   ->with( $siteKey )
		                   ->willThrowException( new InvalidProductBucketUrl( 'error' ) );

		$productsConfigStorage->expects( $this->once() )
		                      ->method( 'get_repository_product_version' )
		                      ->with( $repositoryId )
		                      ->willReturn( 3 );

		$subscriptionManager = new SubscriptionManager( $repositoryId, $installerApiClient, $productsConfigStorage );

		$this->expectException( InvalidProductBucketUrl::class );

		$subscriptionManager->fetch( $siteKey, $source );
	}
}