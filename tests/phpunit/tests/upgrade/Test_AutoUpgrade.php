<?php

namespace OTGS\Installer\Upgrade;

/**
 * @group auto-update
 */
class Test_AutoUpgrade extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function itAddsHooks() {
		$installerPlugins = $this->createMock( InstallerPlugins::class );
		$installerPlugins->method( 'getFilteredInstallerPlugins' )->willReturn( [] );

		$installerPluginsFinder = $this->createMock( 'OTGS_Installer_Plugin_Finder' );
		$installerPluginsFinder->method( 'get_otgs_installed_plugins_by_repository' )->willReturn( [] );
		$subject = $this->getSubject( $installerPluginsFinder, $installerPlugins );

		\WP_Mock::expectFilterAdded( 'pre_update_site_option_auto_update_plugins', [
			$subject,
			'modifyAutoUpdatePluginsOption',
		], 10, 2 );

		$subject->addHooks();
	}

	/**
	 * @test
	 */
	public function itHandlesNonArrayValuesToAutoUpdatePluginsOption() {
		$value    = false;
		$oldValue = [
			'path/installerPlugin2toolset',
			'path/installerPlugin1wpml',
			'path/installerPlugin2wpml',
		];

		$expectedResultValue = [];

		$plugin1Wpml          = 'installerPlugin1wpml';
		$plugin1WpmlPath      = 'path/' . $plugin1Wpml;
		$installerPlugin1wpml = [
			'id'   => $plugin1WpmlPath,
			'name' => $plugin1Wpml,
			'slug' => $plugin1Wpml,
		];

		$installerPluginsList = [
			'wpml'    => [
				$installerPlugin1wpml,
				[
					'id'   => 'path/installerPlugin2wpml',
					'name' => 'InstallerPlugin2wpml',
					'slug' => 'installerPlugin2wpml',
				],
			],
			'toolset' => [
				[
					'id'   => 'path/installerPlugin1toolset',
					'name' => 'InstallerPlugin1toolset',
					'slug' => 'installerPlugin1toolset',
				],
				[
					'id'   => 'path/installerPlugin2toolset',
					'name' => 'InstallerPlugin2toolset',
					'slug' => 'installerPlugin2toolset',
				],
			],
		];

		$installerPluginsFinder = $this->createMock( 'OTGS_Installer_Plugin_Finder' );
		$installerPluginsFinder->method( 'get_otgs_installed_plugins_by_repository' )->willReturn( $installerPluginsList );

		$pluginObj = $this->createMock( 'OTGS_Installer_Plugin' );
		$installerPluginsFinder->method( 'get_plugin' )->with( $plugin1Wpml, 'wpml' )->willReturn( $pluginObj );


		$installerPlugins = $this->createMock( InstallerPlugins::class );
		$installerPlugins->method( 'getFilteredInstallerPlugins' )->willReturn( $installerPluginsList );
		$installerPlugins->method( 'getPluginData' )
			->withConsecutive( [ 'wpml', 'path/installerPlugin2toolset' ], [
				'toolset',
				'path/installerPlugin2toolset'
			] )
			->willReturnOnConsecutiveCalls( $installerPlugin1wpml, null );
		$subject = $this->getSubject( $installerPluginsFinder, $installerPlugins );
		$result  = $subject->modifyAutoUpdatePluginsOption( $value, $oldValue );
		$this->assertEqualsCanonicalizing( $expectedResultValue, $result );
	}

	/**
	 * @test
	 */
	public function itAddsAutoUpdatePluginsOption() {
		$value    = [
			'path/installerPlugin2toolset',
			'path/installerPlugin1wpml',
		];
		$oldValue = [
			'path/installerPlugin2toolset',
		];

		$expectedResultValue = [
			'path/installerPlugin2toolset',
			'path/installerPlugin1wpml',
			'path/installerPlugin2wpml',
		];

		$plugin1Wpml          = 'installerPlugin1wpml';
		$plugin1WpmlPath      = 'path/' . $plugin1Wpml;
		$installerPlugin1wpml = [
			'id'   => $plugin1WpmlPath,
			'name' => $plugin1Wpml,
			'slug' => $plugin1Wpml,
		];

		$installerPluginsList = [
			'wpml'    => [
				$installerPlugin1wpml,
				[
					'id'   => 'path/installerPlugin2wpml',
					'name' => 'InstallerPlugin2wpml',
					'slug' => 'installerPlugin2wpml',
				],
			],
			'toolset' => [
				[
					'id'   => 'path/installerPlugin1toolset',
					'name' => 'InstallerPlugin1toolset',
					'slug' => 'installerPlugin1toolset',
				],
				[
					'id'   => 'path/installerPlugin2toolset',
					'name' => 'InstallerPlugin2toolset',
					'slug' => 'installerPlugin2toolset',
				],
			],
		];

		$installerPluginsFinder = $this->createMock( 'OTGS_Installer_Plugin_Finder' );
		$installerPluginsFinder->method( 'get_otgs_installed_plugins_by_repository' )->willReturn( $installerPluginsList );

		$pluginObj = $this->createMock( 'OTGS_Installer_Plugin' );
		$installerPluginsFinder->method( 'get_plugin' )->with( $plugin1Wpml, 'wpml' )->willReturn( $pluginObj );

		$installerPlugins = $this->createMock( InstallerPlugins::class );
		$installerPlugins->method( 'getFilteredInstallerPlugins' )->willReturn( $installerPluginsList );
		$installerPlugins->method( 'getPluginData' )
		                 ->withConsecutive( [ 'wpml', 'path/installerPlugin1wpml' ], [
			                 'toolset',
			                 'path/installerPlugin1wpml'
		                 ] )
		                 ->willReturnOnConsecutiveCalls( $installerPlugin1wpml, null );
		$subject = $this->getSubject( $installerPluginsFinder, $installerPlugins );

		$result = $subject->modifyAutoUpdatePluginsOption( $value, $oldValue );
		$this->assertEqualsCanonicalizing( $expectedResultValue, $result );
	}

	/**
	 * @test
	 */
	public function itRemovesAutoUpdatePluginsOption() {
		$value    = [
			'path/installerPlugin2toolset',
			'path/installerPlugin2wpml',
		];
		$oldValue = [
			'path/installerPlugin2toolset',
			'path/installerPlugin1wpml',
			'path/installerPlugin2wpml',
		];

		$expectedResultValue = [
			'path/installerPlugin2toolset',
		];

		$plugin1Wpml          = 'installerPlugin1wpml';
		$plugin1WpmlPath      = 'path/' . $plugin1Wpml;
		$installerPlugin1wpml = [
			'id'   => $plugin1WpmlPath,
			'name' => $plugin1Wpml,
			'slug' => $plugin1Wpml,
		];

		$installerPluginsList = [
			'wpml'    => [
				$installerPlugin1wpml,
				[
					'id'   => 'path/installerPlugin2wpml',
					'name' => 'InstallerPlugin2wpml',
					'slug' => 'installerPlugin2wpml',
				],
			],
			'toolset' => [
				[
					'id'   => 'path/installerPlugin1toolset',
					'name' => 'InstallerPlugin1toolset',
					'slug' => 'installerPlugin1toolset',
				],
				[
					'id'   => 'path/installerPlugin2toolset',
					'name' => 'InstallerPlugin2toolset',
					'slug' => 'installerPlugin2toolset',
				],
			],
		];

		$installerPluginsFinder = $this->createMock( 'OTGS_Installer_Plugin_Finder' );
		$installerPluginsFinder->method( 'get_otgs_installed_plugins_by_repository' )->willReturn( $installerPluginsList );

		$pluginObj = $this->createMock( 'OTGS_Installer_Plugin' );
		$installerPluginsFinder->method( 'get_plugin' )->with( $plugin1Wpml, 'wpml' )->willReturn( $pluginObj );


		$installerPlugins = $this->createMock( InstallerPlugins::class );
		$installerPlugins->method( 'getFilteredInstallerPlugins' )->willReturn( $installerPluginsList );
		$installerPlugins->method( 'getPluginData' )
		                 ->withConsecutive( [ 'wpml', 'path/installerPlugin1wpml' ], [
			                 'toolset',
			                 'path/installerPlugin1wpml'
		                 ] )
		                 ->willReturnOnConsecutiveCalls( $installerPlugin1wpml, null );
		$subject = $this->getSubject( $installerPluginsFinder, $installerPlugins );
		$result  = $subject->modifyAutoUpdatePluginsOption( $value, $oldValue );
		$this->assertEqualsCanonicalizing( $expectedResultValue, $result );
	}

	/**
	 * @param \OTGS_Installer_Plugin_Finder $installerPluginsFinder
	 * @param array $installerPlugins
	 *
	 * @return AutoUpgrade
	 */
	private function getSubject( $installerPluginsFinder, $installerPlugins ) {
		$installer = $this->createMock( 'WP_Installer' );
		$subject   = new AutoUpgrade( $installer, $installerPluginsFinder, $installerPlugins );

		return $subject;
	}
}
