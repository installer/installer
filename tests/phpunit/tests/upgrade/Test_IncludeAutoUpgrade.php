<?php

namespace OTGS\Installer\Upgrade;

class Test_IncludeAutoUpgrade extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_includes_plugin_in_auto_update() {
		$currentAutoUpdates = [
			'sitepress-multilingual-cms/sitepress.php',
			'wpml-string-translation/plugin.php',
			'wpml-media-translation/plugin.php',
		];

		\WP_Mock::userFunction( 'get_site_option', [
			'args'   => [ 'auto_update_plugins', [] ],
			'return' => $currentAutoUpdates,
		] );

		$pluginId            = 'gravityforms-multilingual/plugin.php';
		$expectedAutoUpdates = [
			'sitepress-multilingual-cms/sitepress.php',
			'wpml-string-translation/plugin.php',
			'wpml-media-translation/plugin.php',
			$pluginId,
		];

		\WP_Mock::userFunction( 'update_site_option', [
			'args'  => [ 'auto_update_plugins', $expectedAutoUpdates ],
			'times' => 1,
		] );

		$includeAutoUpgrade = $this->getSubject( true );

		$includeAutoUpgrade->includeDuringInstall( $pluginId );

	}

	/**
	 * @test
	 */
	public function it_does_not_include_plugin_in_auto_update() {
		$includeAutoUpgrade = $this->getSubject( false );
		$includeAutoUpgrade->includeDuringInstall( 'plugin-id' );
	}

	/**
	 * @test
	 */
	public function it_stores_unique_plugins_for_auto_upgrade() {
		$includeAutoUpgrade = $this->getSubject( true );

		$pluginId           = 'wpml-media-translation/plugin.php';
		$currentAutoUpdates = [
			'sitepress-multilingual-cms/sitepress.php',
			'wpml-string-translation/plugin.php',
			$pluginId,
		];

		\WP_Mock::userFunction( 'get_site_option', [
			'args'   => [ 'auto_update_plugins', [] ],
			'return' => $currentAutoUpdates,
		] );

		$expectedAutoUpdates = [
			'sitepress-multilingual-cms/sitepress.php',
			'wpml-string-translation/plugin.php',
			$pluginId,
		];

		\WP_Mock::userFunction( 'update_site_option', [
			'args'  => [ 'auto_update_plugins', $expectedAutoUpdates ],
			'times' => 1,
		] );

		$includeAutoUpgrade->includeDuringInstall( $pluginId );
	}

	/**
	 * @test
	 */
	public function it_does_not_include_plugin_in_auto_update_if_setting_does_not_exists() {
		$settings           = [
			'repositories' => [
				'wpml' => [
				],
			],
		];
		$includeAutoUpgrade = new IncludeAutoUpgrade( $settings, 'wpml' );

		$includeAutoUpgrade->includeDuringInstall( 'plugin-id' );
	}

	/**
	 * @param array $settings
	 *
	 * @return IncludeAutoUpgrade
	 */
	private function getSubject( $autoUpdate ) {
		$settings = [
			'repositories' => [
				'wpml' => [
					'auto_update' => $autoUpdate,
				],
			],
		];

		return new IncludeAutoUpgrade( $settings, 'wpml' );
	}

}