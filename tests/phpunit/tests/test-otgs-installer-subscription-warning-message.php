<?php

class Test_Installer_Subscription_Warning_Messages extends OTGS_TestCase {

	const SUBSCRIPTION_ID = 6719;
	const WPML_ORG_WARNING_DAYS = 20;
	const WARNING_MESSAGE_PREFIX = 'Your WPML account expires in 1 day. ';
	const WPML_ORG_WARNING_MSG = 'WPML warning msg: Please enable auto renewal to get updates and support.';
	const INSTALLER_WARNING_MSG = "<a style='margin-left:0px;' href='https://wpml.org/account/?utm_source=plugin&utm_medium=gui&utm_campaign=installer&utm_term=expiring-soon' target='_blank'>Renew today</a> to protect your site from breaking changes in future WordPress releases.<br>";

	protected function getSettingsMock( $mockName ) {
		$settingsMock = [
			'warningMessageFromSubscriptionsMeta' => [
				'repositories' => [
					'wpml'    => [
						'data'         => [
							'url' => 'https://wpml.org',
							'product-name' => 'WPML',
							'subscriptions_meta' => [
								'expiration' => [
									(string) self::SUBSCRIPTION_ID => [
										'days_warning'    => (string) self::WPML_ORG_WARNING_DAYS,
										'warning_message' => self::WPML_ORG_WARNING_MSG
									]
								]
							]
						],
						'subscription' => [
							'data' => [
								'subscription_type' => self::SUBSCRIPTION_ID,
								'status'            => 1,
								'expires'           => ( new DateTime( 'tomorrow' ) )->format( 'Y/m/d' ),
								'hasAutoRenewal'    => false,
							]
						]
					],
					'toolset' => [
						'data'         => [
							'url' => 'https://toolset.com',
							'product-name' => 'toolset',
						],
						'subscription' => [
							'data' => [
								'subscription_type' => self::SUBSCRIPTION_ID
							]
						]
					]
				]
			],
			'warningMessageFromInstaller'         => [
				'repositories' => [
					'wpml'    => [
						'data'         => [
							'url' => 'https://wpml.org',
							'product-name' => 'WPML',
							'subscriptions_meta' => [
								'expiration' => [
									(string) self::SUBSCRIPTION_ID => [
									]
								]
							]
						],
						'subscription' => [
							'data' => [
								'subscription_type' => self::SUBSCRIPTION_ID,
								'status'            => 1,
								'expires'           => ( new DateTime( 'tomorrow' ) )->format( 'Y/m/d' ),
								'hasAutoRenewal'    => false,
							]
						]
					],
					'toolset' => [
						'data'         => [
							'url' => 'https://toolset.com',
							'product-name' => 'toolset',
						],
						'subscription' => [
							'data' => [
								'subscription_type' => self::SUBSCRIPTION_ID
							]
						]
					]
				]
			],
			'autoRenewalEnabled'                  => [
				'repositories' => [
					'wpml'    => [
						'data'         => [
							'url' => 'https://wpml.org',
							'subscriptions_meta' => [
								'expiration' => [
									(string) self::SUBSCRIPTION_ID => [
									]
								]
							]
						],
						'subscription' => [
							'data' => [
								'subscription_type' => self::SUBSCRIPTION_ID,
								'status'            => 1,
								'expires'           => ( new DateTime( 'tomorrow' ) )->format( 'Y/m/d' ),
								'hasAutoRenewal'    => true,
							]
						]
					],
					'toolset' => [
						'data'         => [
							'url' => 'https://toolset.com',
						],
						'subscription' => [
							'data' => [
								'subscription_type' => self::SUBSCRIPTION_ID
							]
						]
					]
				]
			],
		];

		return array_key_exists( $mockName, $settingsMock ) ? $settingsMock[ $mockName ] : [];
	}

	/**
	 * @test
	 */
	public function it_returns_proper_message_from_wpml_org_when_auto_renew_not_active() {
		$wpInstallerMock           = Mockery::mock( WP_Installer::class );
		$wpInstallerMock->settings = $this->getSettingsMock( 'warningMessageFromSubscriptionsMeta' );

		$wpInstallerMock->shouldReceive( 'repository_has_valid_subscription' )
		                ->once()
		                ->andReturnTrue();

		$wpInstallerMock->shouldReceive( 'menu_url' )
		                ->once()
		                ->andReturn( "" );

		$subscriptionWarningMessage = new \OTGS\Installer\Subscription_Warning_Message( $wpInstallerMock );
		$result                     = $subscriptionWarningMessage->get( 'wpml', self::SUBSCRIPTION_ID );

		$this->assertSame( $result, self::WARNING_MESSAGE_PREFIX . self::WPML_ORG_WARNING_MSG );
	}

	/**
	 * @test
	 */
	public function it_returns_proper_message_from_installer_when_auto_renew_not_active() {
		$wpInstallerMock           = Mockery::mock( WP_Installer::class );
		$wpInstallerMock->settings = $this->getSettingsMock( 'warningMessageFromInstaller' );

		$wpInstallerMock->shouldReceive( 'repository_has_valid_subscription' )
		                ->once()
		                ->andReturnTrue();

		$wpInstallerMock->shouldReceive( 'menu_url' )
		                ->once()
		                ->andReturn( "" );

		$subscriptionWarningMessage = new \OTGS\Installer\Subscription_Warning_Message( $wpInstallerMock );
		$result                     = $subscriptionWarningMessage->get( 'wpml', self::SUBSCRIPTION_ID );

		$this->assertSame( $result, self::WARNING_MESSAGE_PREFIX . self::INSTALLER_WARNING_MSG );
	}

	/**
	 * @test
	 */
	public function it_dont_return_any_message_when_auto_renewal_enabled() {
		$wpInstallerMock           = Mockery::mock( WP_Installer::class );
		$wpInstallerMock->settings = $this->getSettingsMock( 'autoRenewalEnabled' );

		$wpInstallerMock->shouldReceive( 'repository_has_valid_subscription' )
		                ->once()
		                ->andReturnTrue();

		$wpInstallerMock->shouldReceive( 'menu_url' )
		                ->once()
		                ->andReturn( "" );

		$subscriptionWarningMessage = new \OTGS\Installer\Subscription_Warning_Message( $wpInstallerMock );

		$this->assertEmpty( $subscriptionWarningMessage->get( 'wpml', self::SUBSCRIPTION_ID ) );
	}
}