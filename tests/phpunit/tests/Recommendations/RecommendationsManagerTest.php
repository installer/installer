<?php

namespace OTGS\Installer\Tests\Recommendations;

use OTGS\Installer\Recommendations\RecommendationsManager;
use OTGS\Installer\Recommendations\Storage;
use PHPUnit\Framework\TestCase;
use tad\FunctionMocker\FunctionMocker;

class RecommendationsManagerTest extends TestCase {

	private $available_plugins_list = [
		'sitepress-multilingual-cms',
		'wpml-string-translation',
		'wpml-ninja-forms',
		'contact-form-7-multilingual',
		'gravityforms-multilingual',
		'slug',
		'slug2',
		'wcml',
	];

	private $installed_plugins_list = [
		'sitepress-multilingual-cms/plugin.php' => [ 'name' => 'Sitepress' ],
		'wpml-string-translation/plugin.php'    => [ 'name' => 'String Translation' ],
		'woocommerce/plugin.php'                => [ 'name' => 'woocommerce' ],
		'wp-import-export-lite/wp-import-export-lite.php'                => [ 'name' => 'WP Import Export Lite' ],
	];

	private $site_key = 'sitekey';
	private $site_url = 'https://exampleurl.com';

	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	/**
	 * @test
	 */
	public function it_gets_repository_plugins_recommendations() {
		$plugin_download_url  = 'https://downloadUrl.com';
		$plugin_download_url2 = 'https://downloadUrl.com2';
		$plugin_download_url3 = 'https://downloadUrl.com3';

		$settings = $this->create_plugins_settings(
			[
				'slug' => [
					'slug'                            => 'slug',
					'download_recommendation_section' => 'group',
					'url'                             => $plugin_download_url,
					'name'                            => 'name',
					'short_description'               => [ 'en' => 'short_description' ],
					'recommended'                     => true,
					'glue_check_type'                 => '',
					'glue_check_value'                => '',
					'recommendation_icon_url'         => 'icon',
				],
				'slug2' => [
					'slug'                            => 'slug2',
					'download_recommendation_section' => 'group',
					'url'                             => $plugin_download_url2,
					'name'                            => 'name2',
					'short_description'               => [ 'en' => 'short_description2' ],
					'recommended'                     => true,
					'glue_check_type'                 => '',
					'glue_check_value'                => '',
					'recommendation_icon_url'         => 'icon2',
				],
				'slug3' => [
					'slug'                            => 'slug3',
					'download_recommendation_section' => 'group3',
					'url'                             => $plugin_download_url3,
					'name'                            => 'name3',
					'short_description'               => [ 'en' => 'short_description3' ],
					'recommended'                     => false,
					'glue_check_type'                 => '',
					'glue_check_value'                => '',
					'glue_check_slug'                => '',
					'glue_check_name'                => '',
					'recommendation_icon_url'         => 'icon3',
				],
			],
			[
				'group'  => [ 'en' => [ 'name' => 'name', 'order' => '1' ] ],
				'group3' => [ 'en' => [ 'name' => 'name', 'order' => '3' ] ],
			],
			[
				'wp-import-export-lite' => [
					'glue_plugin' => 'slug3',
					'glue_check_slug' => 'glue_check_slug',
					'glue_check_name' => 'glue_check_name',
					'recommendation_notification' => ['en' => 'Need to translate forms from Ninja Forms?'],
				],
			]
		);

		$this->mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url );
		$this->mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url2 );
		$this->mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url3 );

		$subject         = $this->prepare_subject( $settings );
		$recommendations = $subject->getRepositoryPluginsRecommendations();

		$this->assertArrayHasKey( 'group', $recommendations['sections'] );
		$this->assertArrayHasKey( 'plugins', $recommendations['sections']['group'] );
		$this->assertArrayHasKey( 'title', $recommendations['sections']['group'] );
		$this->assertArrayHasKey( 'order', $recommendations['sections']['group'] );
		$this->assertArrayHasKey( 'slug', $recommendations['sections']['group']['plugins'] );
		$this->assertEquals(
			[
				'name'                    => 'name',
				'short_description'       => 'short_description',
				'is_installed'            => false,
				'is_active'               => false,
				'slug'                    => 'slug',
				'recommendation_icon_url' => 'icon'
			],
			$recommendations['sections']['group']['plugins']['slug']
		);

		$this->assertEquals(
			[
				'name'                    => 'name',
				'short_description'       => 'short_description',
				'is_installed'            => false,
				'is_active'               => false,
				'slug'                    => 'slug',
				'download_data'           => 'eyJ1cmwiOiJodHRwczpcL1wvZG93bmxvYWRVcmwuY29tP3NpdGVfa2V5PXNpdGVrZXkmc2l0ZV91cmw9aHR0cHM6XC9cL2V4YW1wbGV1cmwuY29tIiwic2x1ZyI6InNsdWciLCJub25jZSI6Im5vbmNlIiwicmVwb3NpdG9yeV9pZCI6IndwbWwifQ==',
				'recommendation_icon_url' => 'icon',
			],
			$recommendations['plugins']['slug']
		);

		$this->assertArrayHasKey( 'slug2', $recommendations['sections']['group']['plugins'] );
		$this->assertEquals(
			[
				'name'                    => 'name2',
				'short_description'       => 'short_description2',
				'is_installed'            => false,
				'is_active'               => false,
				'slug'                    => 'slug2',
				'recommendation_icon_url' => 'icon2',
			],
			$recommendations['sections']['group']['plugins']['slug2']
		);

		$this->assertEquals(
			[
				'name'                    => 'name2',
				'short_description'       => 'short_description2',
				'is_installed'            => false,
				'is_active'               => false,
				'slug'                    => 'slug2',
				'download_data'           => 'eyJ1cmwiOiJodHRwczpcL1wvZG93bmxvYWRVcmwuY29tMj9zaXRlX2tleT1zaXRla2V5JnNpdGVfdXJsPWh0dHBzOlwvXC9leGFtcGxldXJsLmNvbSIsInNsdWciOiJzbHVnMiIsIm5vbmNlIjoibm9uY2UiLCJyZXBvc2l0b3J5X2lkIjoid3BtbCJ9',
				'recommendation_icon_url' => 'icon2',
			],
			$recommendations['plugins']['slug2']
		);
		$this->assertArrayNotHasKey( 'slug3', $recommendations['sections']['group']['plugins'] );
	}

	/**
	 * @test
	 */
	public function it_gets_repository_plugins_recommendations_only_inactive_and_not_installed() {
		$plugin_download_url  = 'https://downloadUrl.com';
		$plugin_download_url2 = 'https://downloadUrl.com2';
		$plugin_download_url3 = 'https://downloadUrl.com3';

		$settings = $this->create_plugins_settings(
			[
				[
					'slug'                            => 'slug',
					'download_recommendation_section' => 'group',
					'url'                             => $plugin_download_url,
					'name'                            => 'name',
					'short_description'               => 'short_description',
					'recommended'                     => true,
					'glue_check_type'                 => '',
					'glue_check_value'                => '',
				],
				[
					'slug'                            => 'slug2',
					'download_recommendation_section' => 'group',
					'url'                             => $plugin_download_url2,
					'name'                            => 'name2',
					'short_description'               => 'short_description2',
					'recommended'                     => true,
					'glue_check_type'                 => '',
					'glue_check_value'                => '',
				],
				[
					'slug'                            => 'slug3',
					'download_recommendation_section' => 'group3',
					'url'                             => $plugin_download_url3,
					'name'                            => 'name3',
					'short_description'               => 'short_description3',
					'recommended'                     => false,
					'glue_check_type'                 => '',
					'glue_check_value'                => '',
				],
				[
					'slug'                            => 'sitepress-multilingual-cms',
					'download_recommendation_section' => 'group',
					'url'                             => $plugin_download_url . 'sitepress-multilingual-cms',
					'name'                            => 'sitepress-multilingual-cms',
					'short_description'               => 'sitepress-multilingual-cms',
					'recommended'                     => true,
					'glue_check_type'                 => '',
					'glue_check_value'                => '',
				],
				[
					'slug'                            => 'wpml-string-translation',
					'download_recommendation_section' => 'group',
					'url'                             => $plugin_download_url . 'wpml-string-translation',
					'name'                            => 'wpml-string-translation',
					'short_description'               => 'wpml-string-translation',
					'recommended'                     => true,
					'glue_check_type'                 => '',
					'glue_check_value'                => '',
				],
			],
			[
				'group'  => [ 'en' => [ 'name' => 'name', 'order' => '1' ] ],
				'group3' => [ 'en' => [ 'name' => 'name', 'order' => '3' ] ],
			]
		);

		\WP_Mock::userFunction( 'is_plugin_active', [
			'args'   => 'sitepress-multilingual-cms/plugin.php',
			'return' => true,
		] );

		\WP_Mock::userFunction( 'is_plugin_active', [
			'args'   => 'wpml-string-translation/plugin.php',
			'return' => false,
		] );

		$this->mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url );
		$this->mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url2 );
		$this->mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url . 'wpml-string-translation' );

		$subject         = $this->prepare_subject( $settings );
		$recommendations = $subject->getRepositoryPluginsRecommendations();

		$this->assertArrayHasKey( 'group', $recommendations['sections'] );
		$this->assertArrayHasKey( 'slug', $recommendations['sections']['group']['plugins'] );
		$this->assertArrayHasKey( 'slug2', $recommendations['sections']['group']['plugins'] );
		$this->assertArrayHasKey( 'wpml-string-translation', $recommendations['sections']['group']['plugins'] );
		$this->assertArrayNotHasKey( 'slug3', $recommendations['sections']['group']['plugins'] );
		$this->assertArrayNotHasKey( 'sitepress-multilingual-cms', $recommendations['sections']['group']['plugins'] );

		$this->assertArrayHasKey( 'slug', $recommendations['plugins'] );
		$this->assertArrayHasKey( 'slug2', $recommendations['plugins'] );
		$this->assertArrayHasKey( 'wpml-string-translation', $recommendations['plugins'] );
		$this->assertArrayNotHasKey( 'slug3', $recommendations['plugins'] );
		$this->assertArrayNotHasKey( 'sitepress-multilingual-cms', $recommendations['plugins'] );
	}

	/**
	 * @test
	 */
	public function it_gets_repository_plugins_recommendations_only_when_glue_might_be_needed() {
		$plugin_download_url  = 'https://wcml_downloadUrl.com';
		$plugin_download_url3 = 'https://downloadUrl.com3';

		$settings = $this->create_plugins_settings(
			[
				[
					'slug'                            => 'wcml',
					'download_recommendation_section' => 'glue',
					'url'                             => $plugin_download_url,
					'name'                            => 'name2',
					'short_description'               => 'short_description2',
					'recommended'                     => true,
					'glue_check_type'                 => 'function',
					'glue_check_value'                => 'woocommerce_function',
				],
				[
					'slug'                            => 'slug3',
					'download_recommendation_section' => 'group3',
					'url'                             => $plugin_download_url3,
					'name'                            => 'name3',
					'short_description'               => 'short_description3',
					'recommended'                     => false,
					'glue_check_type'                 => '',
					'glue_check_value'                => '',
				],
			],
			[
				'glue'   => [ 'en' => [ 'name' => 'name', 'order' => '1' ] ],
				'group3' => [ 'en' => [ 'name' => 'name', 'order' => '3' ] ],
			]
		);

		\WP_Mock::userFunction( 'is_plugin_active', [
			'args'   => 'woocommerce/plugin.php',
			'return' => true,
		] );

		\WP_Mock::userFunction( 'woocommerce_function', [
			'return' => true,
		] );

		$this->mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url );
		$this->mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url3 );

		$subject         = $this->prepare_subject( $settings );
		$recommendations = $subject->getRepositoryPluginsRecommendations();

		$this->assertArrayHasKey( 'glue', $recommendations['sections'] );
		$this->assertArrayNotHasKey( 'group3', $recommendations['sections'] );
		$this->assertArrayHasKey( 'wcml', $recommendations['sections']['glue']['plugins'] );

		$this->assertArrayNotHasKey( 'slug3', $recommendations['plugins'] );
		$this->assertArrayHasKey( 'wcml', $recommendations['plugins'] );
	}

	/**
	 * @test
	 */
	public function it_gets_repository_plugins_recommendations_notices() {
		$plugin_download_url = 'https://downloadUrl.com';

		$pluginSlug         = 'ninja-forms';
		$repositoryId       = 'wpml';
		$recommendationSlug = 'wpml-ninja-forms';
		$notices            = [
			$repositoryId =>
				[
					$pluginSlug =>
						[
							'repository_id'               => '' . $repositoryId . '',
							'glue_check_slug'             => $pluginSlug,
							'glue_check_name'             => 'Ninja Forms',
							'glue_plugin_name'            => 'Ninja Forms <span>Multilingual</span>',
							'glue_plugin_slug'            => $recommendationSlug,
							'recommendation_notification' => 'Need to translate forms from Ninja Forms?',
							'download_data'               =>
								[
									'url'           => $plugin_download_url,
									'slug'          => $recommendationSlug,
									'repository_id' => $repositoryId,
								],
						],
				],
		];

		$this->mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url );

		\WP_Mock::userFunction( 'get_option', [
			'args'   => [ Storage::ADMIN_NOTICES_OPTION, [] ],
			'return' => $notices,
		] );

		$subject = $this->prepare_subject( [] );

		$result = $subject->getRecommendationStoredNotices( [] );

		$this->assertArrayHasKey( 'download_data', $result[ $repositoryId ][ $pluginSlug ] );
		$this->assertEquals( 'nonce', $result[ $repositoryId ][ $pluginSlug ]['download_data']['nonce'] );
		$this->assertEquals(
			'https://downloadUrl.com?site_key=sitekey&site_url=https://exampleurl.com',
			$result[ $repositoryId ][ $pluginSlug ]['download_data']['url'] );
	}

	private function create_plugins_settings( $plugins, $sections, $glueMapping = [] ) {
		return [
			'wpml' => [
				'data' => [
					'downloads'               => [
						'plugins' => $plugins,
					],
					'recommendation_sections' => $sections,
					'glue_plugins_mapping' => $glueMapping
				],
			],
		];
	}

	private function prepare_subject( $settings, $storage = null ) {

		$repositories = $this->createMock( \OTGS_Installer_Repositories::class );
		$repository   = $this->createMock( \OTGS_Installer_Repository::class );
		$subscription = $this->createMock( \OTGS_Installer_Subscription::class );
		$product      = $this->createMock( \OTGS_Installer_Package_Product::class );
		$storage      = $storage ? $storage : $this->createMock( Storage::class );

		$repositories->method( 'get' )->with( 'wpml' )->willReturn( $repository );
		$repository->method( 'get_subscription' )->willReturn( $subscription );
		$repository->method( 'get_product_by_subscription_type' )->willReturn( $product );
		$product->method( 'get_plugins' )->willReturn( $this->available_plugins_list );

		$subscription->method( 'get_site_key' )->willReturn( $this->site_key );
		$subscription->method( 'get_site_url' )->willReturn( $this->site_url );

		\WP_Mock::userFunction( 'get_plugins', [
			'return' => $this->installed_plugins_list,
		] );

		\WP_Mock::userFunction( 'is_plugin_active', [
			'args'   => 'sitepress-multilingual-cms/plugin.php',
			'return' => true,
		] );

		\WP_Mock::userFunction( 'is_plugin_active', [
			'args'   => 'wpml-string-translation/plugin.php',
			'return' => true,
		] );

		\WP_Mock::userFunction( 'is_plugin_active', [
			'args'   => 'wp-import-export-lite/wp-import-export-lite.php',
			'return' => true,
		] );

		\WP_Mock::userFunction( 'is_plugin_active', [
			'args'   => 'woocommerce/plugin.php',
			'return' => false,
		] );

		return new RecommendationsManager( $repositories, $settings, $storage );
	}

	private function mock_wp_functions_for_preparing_plugin_recommendation_data( $plugin_download_url ) {
		\WP_Mock::userFunction( 'add_query_arg', [
			'args'   => [ [ 'site_key' => $this->site_key, 'site_url' => $this->site_url ], $plugin_download_url ],
			'return' => sprintf(
				"%s?site_key=%s&site_url=%s",
				$plugin_download_url,
				$this->site_key,
				$this->site_url
			),
		] );

		\WP_Mock::userFunction( 'wp_create_nonce', [
			'args'   => sprintf(
				"install_plugin_%s?site_key=%s&site_url=%s",
				$plugin_download_url,
				$this->site_key,
				$this->site_url
			),
			'return' => 'nonce',
		] );
	}
}
