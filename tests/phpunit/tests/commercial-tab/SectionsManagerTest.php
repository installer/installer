<?php

namespace OTGS\Installer\Tests\CommercialTab;

use OTGS\Installer\CommercialTab\SectionsManager;

class SectionsManagerTest extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_returns_sections_with_downloads() {
		$repositoryId = 'repositoryId';

		$installed_plugins_list = [
			'sitepress-multilingual-cms/plugin.php' => [ 'name' => 'Sitepress' ],
			'wpml-string-translation/plugin.php'    => [ 'name' => 'String Translation' ],
		];

		\WP_Mock::userFunction( 'get_plugins', [
			'return' => $installed_plugins_list,
		] );

		$downloads = [
			[
				'slug'                            => 'sitepress-multilingual-cms',
				'download_commercial_tab_section' => 'general',
			],
			[
				'slug'                            => 'wpml-string-translation',
				'download_commercial_tab_section' => 'legacy',
			],
		];

		$sections = [
			'general' => [
				'en' => [
					'name'  => 'General',
					'order' => '0'
				],
				'jp' => [
					'name'  => 'GeneralJP',
					'order' => '0'
				]
			],
			'legacy'  => [
				'en' => [
					'name'  => 'Legacy',
					'order' => '3'
				],
				'jp' => [
					'name'  => 'LegacyJP',
					'order' => '3'
				]
			]
		];

		$subject = $this->prepareSubject( $repositoryId, $sections );

		$this->assertEqualsCanonicalizing(
			[
				'general' => [
					'name'      => 'General',
					'order'     => '0',
					'downloads' => [
						[
							'slug'                            => 'sitepress-multilingual-cms',
							'download_commercial_tab_section' => 'general',
						],
					]
				],
				'legacy'  => [
					'name'      => 'Legacy',
					'order'     => '3',
					'downloads' => [
						[
							'slug'                            => 'wpml-string-translation',
							'download_commercial_tab_section' => 'legacy',
						],
					]
				]
			],
			$subject->getPluginsSections( $repositoryId, $downloads )
		);
	}

	/**
	 * @test
	 */
	public function it_returns_sections_with_downloads_with_only_installed_legacy_plugins() {
		$repositoryId = 'repositoryId';

		$installed_plugins_list = [
			'sitepress-multilingual-cms/plugin.php' => [ 'name' => 'Sitepress' ],
		];

		\WP_Mock::userFunction( 'get_plugins', [
			'return' => $installed_plugins_list,
		] );

		$downloads = [
			[
				'slug'                            => 'sitepress-multilingual-cms',
				'download_commercial_tab_section' => 'general',
			],
			[
				'slug'                            => 'wpml-string-translation',
				'download_commercial_tab_section' => 'legacy',
			],
		];

		$sections = [
			'general' => [
				'en' => [
					'name'  => 'General',
					'order' => '0'
				],
				'jp' => [
					'name'  => 'GeneralJP',
					'order' => '0'
				]
			],
			'legacy'  => [
				'en' => [
					'name'  => 'Legacy',
					'order' => '3'
				],
				'jp' => [
					'name'  => 'LegacyJP',
					'order' => '3'
				]
			]
		];

		$subject = $this->prepareSubject( $repositoryId, $sections );

		$this->assertEqualsCanonicalizing(
			[
				'general' => [
					'name'      => 'General',
					'order'     => '0',
					'downloads' => [
						[
							'slug'                            => 'sitepress-multilingual-cms',
							'download_commercial_tab_section' => 'general',
						],
					]
				],
				'legacy'  => [
					'name'      => 'Legacy',
					'order'     => '3',
					'downloads' => []
				]
			],
			$subject->getPluginsSections( $repositoryId, $downloads )
		);
	}

	/**
	 * @test
	 */
	public function it_returns_general_section_when_no_config_found() {
		$repositoryId = 'repositoryId';

		$installed_plugins_list = [
			'sitepress-multilingual-cms/plugin.php' => [ 'name' => 'Sitepress' ],
		];

		\WP_Mock::userFunction( 'get_plugins', [
			'return' => $installed_plugins_list,
		] );

		$downloads = [
			[
				'slug' => 'sitepress-multilingual-cms',
			],
			[
				'slug' => 'wpml-string-translation',
			],
		];

		$subject = $this->prepareSubject( $repositoryId, [] );

		$this->assertEqualsCanonicalizing(
			[
				'general' => [
					'downloads' => [
						[
							'slug'                            => 'sitepress-multilingual-cms',
							'download_commercial_tab_section' => 'general',
						],
						[
							'slug'                            => 'wpml-string-translation',
							'download_commercial_tab_section' => 'general',
						],
					]
				],
			],
			$subject->getPluginsSections( $repositoryId, $downloads )
		);
	}

	public function prepareSubject( $repositoryId, $sections ) {
		$settings = [
			$repositoryId => [
				'data' => [
					'commecial_tab_sections' => $sections
				]
			]
		];

		return new SectionsManager( $settings );
	}
}
